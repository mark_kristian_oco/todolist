import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  Modal,
  ActivityIndicator,
  SafeAreaView,
} from "react-native";
import { colors } from "./config/theme";
import { AntDesign, SimpleLineIcons, MaterialCommunityIcons } from "@expo/vector-icons";
import TodoList from "./components/TodoList";
import AddListModal from "./components/AddListModal";
import Fire from "./Fire";

let firebase;
export default class App extends Component {
  state = {
    addTodoVisible: false,
    lists: [],
    user: {},
    loading: true,
  };

  componentDidMount() {
    firebase = new Fire((error, user) => {
      if (error) {
        return alert("Uh oh! something went wrong!");
      }

      firebase.getLists((listsdb) => {
        this.setState({ lists: listsdb }, () => {
          this.setState({ loading: false });
        });
      });

      this.setState({ user });
    });
  }

  componentWillUnmount() {
    firebase.detach();
  }

  toggleAddTodoModal() {
    this.setState({ addTodoVisible: !this.state.addTodoVisible });
  }

  addList = (list) => {
    firebase.addList({
      name: list.name,
      color: list.color,
      todos: [],
    });
  };

  updateList = (list) => {
    firebase.updateList(list);
  };

  deleteTodo = (list) => {
    firebase.deleteTodo(list);
  };

  render() {
    if (this.state.loading) {
      return (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color={colors.blue} />
        </View>
      );
    }
    return (
      <SafeAreaView style={styles.container}>
        <Modal
          animationType="slide"
          visible={this.state.addTodoVisible}
          onRequestClose={() => this.toggleAddTodoModal()}
        >
          <AddListModal
            addList={this.addList}
            closeModal={() => this.toggleAddTodoModal()}
          />
        </Modal>

       

        <View style={{ flexDirection: "row", marginTop: 8 }}>
          <View style={styles.divider} />
          <Text style={styles.title}>
            Todo{" "}
            <Text style={{ fontWeight: "300", color: colors.blue }}>Lists</Text>
          </Text>
          <View style={styles.divider} />
        </View>

        <View style={{ flexDirection: "row", alignSelf: "flex-end", alignItems: "center" }}>
          <TouchableOpacity style={{margin: 4}}>
            <SimpleLineIcons name="grid" size={24} color={colors.blue}/>
          </TouchableOpacity>
          <TouchableOpacity style={{margin: 4}}>
          <MaterialCommunityIcons name="format-list-bulleted" size={32} color={colors.blue} />
          </TouchableOpacity>
        </View>

        <View style={[{ flex: 1 }]}>
          <FlatList
            data={this.state.lists}
            keyExtractor={(item) => item.id.toString()}
            numColumns={2}
            renderItem={({ item }) => (
              <TodoList
                list={item}
                deleteTodo={this.deleteTodo}
                updateList={this.updateList}
              />
            )}
            keyboardShouldPersistTaps="always"
          />
          <TouchableOpacity
            onPress={() => this.toggleAddTodoModal()}
            style={styles.addBtnContainer}
          >
            <AntDesign name="plus" size={24} color={colors.blue} />
            <Text style={styles.add}>New List</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  divider: {
    backgroundColor: colors.lightBlue,
    height: 1,
    flex: 1,
    alignSelf: "center",
  },
  title: {
    fontSize: 38,
    fontWeight: "800",
    color: colors.black,
    paddingHorizontal: 64,
  },
  addList: {
    alignItems: "center",
    justifyContent: "center",
  },
  add: {
    color: colors.blue,
    fontWeight: "600",
    fontSize: 14,
  },
  addBtnContainer: {
    position: "absolute",
    bottom: 4,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 32,
    zIndex: 2,
    backgroundColor: "#fff",
    borderRadius: 50,
    paddingVertical: 4,
    shadowColor: colors.gray,
    shadowOffset: { height: 1 },
    shadowOpacity: 0.3,
  },
});
