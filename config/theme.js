import { Dimensions } from "react-native";

const { width, height } = Dimensions.get('window');

export const WIDTH = width;
export const HEIGHT = height;

export const colors =  {
  black : "#2D3436",
  gray: "#A4A4A4",
  lightGray: "#CACACA",
  blue: "#24A6D9",
  lightBlue: "#A7CBD9",
  white: "#FFFFFF",
  red: "#D85963"
}