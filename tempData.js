export const tempData = [
  {
    id: 1,
    name: "Plan a Trip",
    color: "#24A6D9",
    todos: [
      {
        title: "Book Flight",
        completed: false
      },
      {
        title: "Passport Check",
        completed: true
      },
      {
        title: "Reserve Hotel Room",
        completed: false
      },
      {
        title: "Pack Luggage",
        completed: false
      },
    ]
  },
  {
    id: 2,
    name: "Go Diving",
    color: "#8022D9",
    todos: [
      {
        title: "Complete Diving Lesson",
        completed: false
      },
      {
        title: "Buy Diving Equipments",
        completed: true
      },
    ]
  }
]