import {
  MaterialCommunityIcons,
  Ionicons,
  Fontisto,
} from "@expo/vector-icons";
import React from "react";
import { StyleSheet, View, Text, TouchableOpacity, Modal } from "react-native";
import { colors, WIDTH } from "../config/theme";
import TodoModal from "./TodoModal";

export default class TodoList extends React.Component {
  state = {
    showListVisible: false,
  };

  toggleListModal() {
    this.setState({ showListVisible: !this.state.showListVisible });
  }

  render() {
    const list = this.props.list;

    const completedCount = list.todos.filter((todo) => todo.completed).length;
    const remainingCount = list.todos.length - completedCount;

    return (
      <View>
        <Modal
          animationType="slide"
          visible={this.state.showListVisible}
          onRequestClose={() => this.toggleListModal()}
        >
          <TodoModal
            closeModal={() => this.toggleListModal()}
            list={list}
            updateList={this.props.updateList}
          />
        </Modal>
        <TouchableOpacity
          onPress={() => this.toggleListModal()}
          style={[styles.listContainer, { backgroundColor: list.color }]}
        >
          <TouchableOpacity
            style={styles.iconContainer}
            onPress={() => this.props.deleteTodo(list)}
          >
            <MaterialCommunityIcons
              name="close"
              size={24}
              color={colors.white}
            />
          </TouchableOpacity>
          <Text style={styles.listTitle} numberOfLines={1}>
            {list.name ? list.name : "Untitled"}
          </Text>
          <View>
            <View style={{ alignItems: "center" }}>
              <Text style={styles.count}>{remainingCount}</Text>
              <Text style={styles.subtitle}>Remaining</Text>
            </View>
            <View style={{ alignItems: "center" }}>
              <Text style={styles.count}>{completedCount}</Text>
              <Text style={styles.subtitle}>Completed</Text>
            </View>
          </View>
          <View style={styles.iconBottomContainer}>
            <TouchableOpacity
              style={styles.icon}
              onPress={() =>
                this.props.updateList({ ...list, favorite: !list.favorite })
              }
            >
              <Ionicons name="share-social-outline" size={24} color={colors.white} />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.icon}
              onPress={() =>
                this.props.updateList({ ...list, favorite: !list.favorite })
              }
            >
              <Fontisto
                name={list.favorite ? "bookmark-alt" : "bookmark"}
                size={24}
                color={colors.white}
              />
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderRadius: 6,
    alignItems: "center",
    width: WIDTH/2 - 8,
    margin: 2
  },
  listTitle: {
    fontSize: 24,
    fontWeight: "700",
    color: colors.white,
    marginBottom: 18,
  },
  count: {
    fontSize: 48,
    fontWeight: "200",
    color: colors.white,
  },
  subtitle: {
    fontSize: 12,
    fontWeight: "700",
    color: colors.white,
  },
  iconContainer: {
    flexDirection: "row",
    alignSelf: "flex-end",
  },
  iconBottomContainer: {
    flexDirection: "row",
    alignSelf: "flex-start",
    marginTop: 8
  },
  icon: {
    paddingRight: 18,
  },
});
