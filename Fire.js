import firebase from "firebase";
import "@firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCCp-6iMy_cqdZA6mTPCMfvZhbogt1Yl_w",
  authDomain: "todoapp-9be3b.firebaseapp.com",
  projectId: "todoapp-9be3b",
  storageBucket: "todoapp-9be3b.appspot.com",
  messagingSenderId: "329727062958",
  appId: "1:329727062958:web:431b21574afb83c585ce3f",
};

class Fire {
  constructor(callback) {
    this.init(callback);
  }

  init(callback) {
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        callback(null, user);
      } else {
        firebase
          .auth()
          .signInAnonymously()
          .catch((error) => {
            callback(error);
          });
      }
    });
  }

  getLists(callback) {
    let ref = this.ref.orderBy("name");

    this.unsubscribe = ref.onSnapshot((snapshot) => {
      let lists = [];
      snapshot.forEach((doc) => {
        lists.push({ id: doc.id, ...doc.data() });
      });
      callback(lists);
    });
  }

  addList(list) {
    let ref = this.ref

    ref.add(list);
  }

  updateList(list) {
    let ref = this.ref;
    
    ref.doc(list.id).update(list)
  }

  deleteTodo (list) {
    let ref = this.ref;
    
    ref.doc(list.id).delete(list)
  }

  get userId() {
    return firebase.auth().currentUser.uid;
  }

  get ref(){
    return firebase
    .firestore()
    .collection("users")
    .doc(this.userId)
    .collection("lists");
  }

  detach() {
    this.unsubscribe();
  }
}

export default Fire;
